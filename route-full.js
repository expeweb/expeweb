//BOILERPLATE CODE TO INITIALIZE THE MAP
const platform = new H.service.Platform({
    'apikey': '5kYXjAMz80NoPISGub0vFLLjW6v7qEjHuV8kCJs1s7w'
});

// Obtain the default map types from the platform object:
const defaultLayers = platform.createDefaultLayers();

// Instantiate (and display) a map:
var map = new H.Map(
    document.getElementById("map"),
    defaultLayers.vector.normal.map, {
        zoom: 6,
        center: {
            lat: 47.220,
            lng: 2.417
        }
    });


// Location popup on click
function setUpClickListener(map) {
  // Attach an event listener to map display
  // obtain the coordinates and display in an alert box.
  map.addEventListener('tap', function (evt) {
    var coord = map.screenToGeo(evt.currentPointer.viewportX,
            evt.currentPointer.viewportY);
    logEvent('Clicked at ' + Math.abs(coord.lat.toFixed(4)) +
        ((coord.lat > 0) ? 'N' : 'S') +
        ' ' + Math.abs(coord.lng.toFixed(4)) +
         ((coord.lng > 0) ? 'E' : 'W'));
  });
}


// map.addEventListener('tap', function(ev) {
//     const target = ev.target;
//     const pointer = ev.currentPointer;
//     const coords = map.screenToGeo(pointer.viewportX, pointer.viewportY);

//     if (!(target instanceof H.map.Marker)) {
//         const marker = addMarker(coords, waypoints.length + 1);
//         waypoints.push(marker);
//         updateRoute();
//     }
// });


// Step 4: create custom logging facilities
var logContainer = document.createElement('ul');
logContainer.className ='log';
logContainer.innerHTML = '<li class="log-entry">Try clicking on the map</li>';
map.getElement().appendChild(logContainer);

// Helper for logging events
function logEvent(str) {
  var entry = document.createElement('li');
  entry.className = 'log-entry';
  entry.textContent = str;
  logContainer.insertBefore(entry, logContainer.firstChild);
}


setUpClickListener(map);

// MapEvents enables the event system
// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

// Disable zoom on double-tap to allow removing waypoints on double-tap
behavior.disable(H.mapevents.Behavior.Feature.DBL_TAP_ZOOM);

window.addEventListener('resize', () => map.getViewPort().resize());

// Create the default UI:
var ui = H.ui.UI.createDefault(map, defaultLayers, );

// ROUTING LOGIC STARTS HERE

// This variable holds the instance of the route polyline
let routePolyline;

function routeResponseHandler(response) {
    const sections = response.routes[0].sections;
    const lineStrings = [];
    sections.forEach((section) => {
        // convert Flexible Polyline encoded string to geometry
        lineStrings.push(H.geo.LineString.fromFlexiblePolyline(section.polyline));
    });
    const multiLineString = new H.geo.MultiLineString(lineStrings);
    const bounds = multiLineString.getBoundingBox();

    // Create the polyline for the route
    if (routePolyline) {
        // If the routePolyline we just set has the new geometry
        routePolyline.setGeometry(multiLineString);
    } else {
        // If routePolyline is not yet defined, instantiate a new H.map.Polyline
        routePolyline = new H.map.Polyline(multiLineString, {
            style: {
                lineWidth: 5
            }
        });
    }

    // Add the polyline to the map
    map.addObject(routePolyline);
}

function getMarkerIcon(id) {
    const svgCircle = `<svg width="30" height="30" version="1.1" xmlns="http://www.w3.org/2000/svg">
  <g id="marker">
    <circle cx="15" cy="15" r="10" fill="#0099D8" stroke="#0099D8" stroke-width="4" />
    <text x="50%" y="50%" text-anchor="middle" fill="#FFFFFF" font-family="Arial, sans-serif" font-size="12px" dy=".3em">${id}</text>
  </g></svg>`;
    return new H.map.Icon(svgCircle, {
        anchor: {
            x: 10,
            y: 10
        }
    });
}

function addMarker(position, id) {
    const marker = new H.map.Marker(position, {
        data: {
            id
        },
        icon: getMarkerIcon(id),
        // Enable smooth dragging
        volatility: true
    });

    // Enable draggable markers
    marker.draggable = true;

    map.addObject(marker);
    return marker;
}

function updateRoute() {
    routingParams.via = new H.service.Url.MultiValueQueryParameter(
        waypoints.map(wp => `${wp.getGeometry().lat},${wp.getGeometry().lng}`));

    // Call the routing service with the defined parameters
    router.calculateRoute(routingParams, routeResponseHandler, console.error);
}

// ADD MARKERS FOR ORIGIN/DESTINATION
const origin = {
    lat: 44.8443,
    lng: -0.5780
};
const destination = {
    lat: 48.8520,
    lng: 2.3459
};

const originMarker = addMarker(origin, 'A');
const destinationMarker = addMarker(destination, 'B');

// CALCULATE THE ROUTE BETWEEN THE TWO WAYPOINTS
// This array holds instances of H.map.Marker representing the route waypoints
const waypoints = []

// Define the routing service parameters
const routingParams = {
    'origin': `${origin.lat},${origin.lng}`,
    'destination': `${destination.lat},${destination.lng}`,
    // defines multiple waypoints
    'via': new H.service.Url.MultiValueQueryParameter(waypoints),
    'transportMode': 'car',
    'return': 'polyline'
};

// Get an instance of the H.service.RoutingService8 service
const router = platform.getRoutingService(null, 8);

// Call the routing service with the defined parameters and display the route
updateRoute();

map.addEventListener('dragstart', function(ev) {
    const target = ev.target;
    const pointer = ev.currentPointer;
    if (target instanceof H.map.Marker) {
        // Disable the default draggability of the underlying map
        behavior.disable(H.mapevents.Behavior.Feature.PANNING);

        var targetPosition = map.geoToScreen(target.getGeometry());
        // Calculate the offset between mouse and target's position
        // when starting to drag a marker object
        target['offset'] = new H.math.Point(
            pointer.viewportX - targetPosition.x, pointer.viewportY - targetPosition.y);
    }
}, false);

map.addEventListener('dragend', function(ev) {
    const target = ev.target;
    if (target instanceof H.map.Marker) {
        // re-enable the default draggability of the underlying map
        // when dragging has completed
        behavior.enable(H.mapevents.Behavior.Feature.PANNING);
        const coords = target.getGeometry();
        const markerId = target.getData().id;

        // Update the routing params `origin` and `destination` properties
        // in case we dragging either the origin or the destination marker
        if (markerId === 'A') {
            routingParams.origin = `${coords.lat},${coords.lng}`;
        } else if (markerId === 'B') {
            routingParams.destination = `${coords.lat},${coords.lng}`;
        }

        updateRoute();
    }
}, false);

map.addEventListener('drag', function(ev) {
    const target = ev.target;
    const pointer = ev.currentPointer;
    if (target instanceof H.map.Marker) {
        target.setGeometry(
            map.screenToGeo(pointer.viewportX - target['offset'].x, pointer.viewportY - target['offset'].y)
        );
    }
}, false);

map.addEventListener('dbltap', function(ev) {
    const target = ev.target;

    if (target instanceof H.map.Marker) {
        // Prevent origin or destination markers from being removed
        if (['origin', 'destination'].indexOf(target.getData().id) !== -1) {
            return;
        }

        const markerIdx = waypoints.indexOf(target);
        if (markerIdx !== -1) {
            // Remove the marker from the array of way points
            waypoints.splice(markerIdx, 1)
            // Iterate over the remaining waypoints and update their data
            waypoints.forEach((marker, idx) => {
                const id = idx + 1;
                // Update marker's id
                marker.setData({
                    id
                });
                // Update marker's icon to show its new id
                marker.setIcon(getMarkerIcon(id))
            });
        }

        // Remove the marker from the map
        map.removeObject(target);

        updateRoute();
    }
});
