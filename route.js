// Instantiate a map and platform object:
var platform = new H.service.Platform({
    'apikey': '5kYXjAMz80NoPISGub0vFLLjW6v7qEjHuV8kCJs1s7w'
});

// Retrieve the target element for the map:
var targetElement = document.getElementById('mapContainer');

// Get the default map types from the platform object:
var defaultLayers = platform.createDefaultLayers();

// Instantiate the map:
var map = new H.Map(
    document.getElementById('mapContainer'),
    defaultLayers.vector.normal.map,
    {
        zoom: 10,
        center: { lat: 52.51, lng: 3.4 }
    });

// Remove or comment out the following line of code to make a map static:
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

 // Enable dynamic resizing of the map, based on the current size of the enclosing cntainer
window.addEventListener('resize', () => map.getViewPort().resize());

// Create the default UI:
const ui = H.ui.UI.createDefault(map, defaultLayers, `fr-FR`);
const mapSettingsControl = ui.getControl('mapsettings');
mapSettingsControl.setAlignment('top-left');

// Create a map layer for the overview control
const overviewLayers = platform.createDefaultLayers();
// Instatiate the overview map by using the H.ui.Overview constructor
const overviewMap = new H.ui.Overview(overviewLayers.raster.satellite.map, {
    //Set the control position and the map size and zoom parameters with respect to the map's viewport
    alignment: H.ui.LayoutAlignment.LEFT_BOTTOM,
    zoomDelta: 6,
    scaleX: 5,
    scaleY: 6
});
// Add the control to the map
ui.addControl('overview', overviewMap);

var routingParameters = {
    'routingMode': 'fast',
    'transportMode': 'truck',
    'origin': '45.54428,3.24668',
    'destination': '42.54567,-2.6645',
    'avoid[zoneCategories]': 'environmental',
    'via':'45.85522,3.54824',
    // 'via':'42.54567,-2.6645',
    // 'via':'',
    // 'avoidAreas': '50.62105096834548,3.0676392931685683',
    'return': 'polyline'
};

// // Enable the event system on the map instance:
// var mapEvents = new H.mapevents.MapEvents(map);

// // Add event listener:
// map.addEventListener('tap', function(evt) {
//     // Log 'tap' and 'mouse' events:
//     console.log(evt.type, evt.currentPointer.type);
// });

// Define a callback function to process the routing response:
var onResult = function(result) {
    console.log('result: ', result);
    // ensure that at least one route was found
    if (result.routes.length) {
        result.routes[0].sections.forEach((section) => {
            // Create a linestring to use as a point source for the route line
            let linestring = H.geo.LineString.fromFlexiblePolyline(section.polyline);

            // Create a polyline to display the route:
            let routeLine = new H.map.Polyline(linestring, {
                style: { strokeColor: 'blue', lineWidth: 3 }
            });

            // Create a marker for the start point:
            let startMarker = new H.map.Marker(section.departure.place.location);

            // Create a marker for the end point:
            let endMarker = new H.map.Marker(section.arrival.place.location);

            // Add the route polyline and the two markers to the map:
            map.addObjects([routeLine, startMarker, endMarker]);

            // Set the map's viewport to make the whole route visible:
            map.getViewModel().setLookAtData({bounds: routeLine.getBoundingBox()});
        });
    }
};

// Get an instance of the routing service version 8:
var router = platform.getRoutingService(null, 8);

// Call calculateRoute() with the routing parameters,
// the callback and an error callback function (called if a
// communication error occurs):
router.calculateRoute(routingParameters, onResult,
                      function(error) {
                          alert(error.message);
                      });
